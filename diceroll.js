function init() {
    let rollButton = document.getElementById("Button");
    rollButton.addEventListener("click", roll);
}
function roll() {
    let count = 12;
    let totals = new Array(count);
    for (let i = 0; i < count; i++) {
        totals[i] = 0;
    }

    let numberOfRolls = parseInt(document.getElementById("numberRolls").value);
    for (let i = 0; i < numberOfRolls; i++) {

        let a = rollDice();
        let b = rollDice();
        totals[(a + b) - 1]++;
    }

    generateResultTable(totals, count);
    generateResultBarGraph(totals, count, numberOfRolls);
}

function rollDice() {
    let dieValue = Math.floor(Math.random() * 6) + 1;
    return dieValue;
}
function generateResultTable(totals, count) {
    let tableHTML = "<table>";
    for (let i = 1; i < count; i++) {
        tableHTML += "<tr><td>";
        tableHTML += i + 1;
        tableHTML += "</td>";
        tableHTML += "<td>";
        tableHTML += totals[i];
        tableHTML += "</td></tr>";
    }

    tableHTML += "</table>";
    let resultTable = document.getElementById("resultTable");
    resultTable.innerHTML = tableHTML;
}

function generateResultBarGraph(totals, count, numberOfRolls) {
    let graphHTML = "";
    for (let i = 1; i < count; i++) {
        graphHTML += "<div id='bar" + i + "' class='bar'></div>";
        graphHTML += "<div id='label" + i + "' class='label'>" + (i + 1) + "</div>";
    }

    let barGraph = document.getElementById("Graph");
    barGraph.innerHTML = graphHTML;å

    for (let i = 1; i < count; i++) {
        let bar = document.getElementById("bar" + i);
        bar.style.left = 100 * i + 100 + "px";
        let label = document.getElementById("label" + i);
        label.style.left = 100 * i + 100 + "px";
        let height = totals[i] / numberOfRolls * 2000;
        bar.style.height = height + "px";
    }
}

window.addEventListener("load", init);
